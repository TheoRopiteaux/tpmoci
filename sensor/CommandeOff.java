/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sensor;


/**
 *
 * @author Théo
 */
public class CommandeOff implements Commande{

    ISensor s;
    
    public CommandeOff(ISensor s){
        this.s=s;
    }
    @Override
    public double execute() {
        s.off();
        return 0;
    }

    @Override
    public void setSensor(ISensor s) {
       this.s=s;
    }
    
}
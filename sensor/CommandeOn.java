/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sensor;


/**
 *
 * @author Théo
 */
public class CommandeOn implements Commande{

    ISensor s;
    
    public CommandeOn(ISensor s){
        this.s=s;
    }
    @Override
    public double execute() {
        s.on();
        return 0;
    }
     @Override
    public void setSensor(ISensor s) {
       this.s=s;
    }
}

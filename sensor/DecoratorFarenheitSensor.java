/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sensor;

/**
 *
 * @author Théo
 */
public class DecoratorFarenheitSensor extends AbstractDecoratorSensor{

    public DecoratorFarenheitSensor(ISensor s) {
        c=s;
    }

    
    @Override
    public double getValue() throws SensorNotActivatedException {
        return super.getValue()*1.8+32; 
    }
    
}

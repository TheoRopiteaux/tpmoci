/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sensor;

/**
 *
 * @author Théo
 */
public class StateOff implements SensorState{

	private TemperatureSensor temp;
        private SensorState On;
	public StateOff(TemperatureSensor sens){
		this.temp = sens;
                
	}
        
        public void setOn(SensorState On){
            this.On=On;
        }
	@Override
	public void on() {
            temp.setState(On);
        }

	@Override
	public void off() {
		
	}

	@Override
	public SensorState getStatus() {
		return this;
	}

}
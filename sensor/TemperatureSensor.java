package sensor;

import java.util.Observable;
import java.util.Random;
import ui.SensorView;

public class TemperatureSensor extends Observable implements ISensor {
    SensorState state;
    StateOn stateOn;
    StateOff stateOff;
    double value = 0;
    
     public TemperatureSensor(){
        
        stateOff = new StateOff(this);
        state=stateOff;
    	stateOn = new StateOn(this);
        stateOff.setOn(stateOn);
    	stateOn.setOff(stateOff);
    }
    
    @Override
    public void on() {
        state.on();
    }
    
    @Override
    public void off() {
        state.off();
    }

    @Override
    public boolean getStatus() {
        return (state.getStatus()==stateOn);
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (this.getStatus()){
            value = (new Random()).nextDouble() * 100;
            setChanged();
            notifyObservers();
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (this.getStatus())
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

    @Override
    public void addObserver1(SensorView aThis) {
        this.addObserver(aThis);
    }
    
    public void setState(SensorState s){
        state=s;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sensor;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Théo
 */
public class CommandeUpdate implements Commande{

    ISensor s;
    
    public CommandeUpdate(ISensor s){
        this.s=s;
    }
    @Override
    public double execute() {
        try {
            s.update();
        } catch (SensorNotActivatedException ex) {
            Logger.getLogger(CommandeUpdate.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
     @Override
    public void setSensor(ISensor s) {
       this.s=s;
    }
}

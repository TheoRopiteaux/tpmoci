/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sensor;

/**
 *
 * @author Théo
 */
public class Invoker {
    Commande cmd;

    public Invoker() {
    }

    public Invoker(Commande cmd) {
        this.cmd = cmd;
    }

    public void setCmd(Commande cmd) {
        this.cmd = cmd;
    }
    
    public double use(){
       return cmd.execute();
    }
}

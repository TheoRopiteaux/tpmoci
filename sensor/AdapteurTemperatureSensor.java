package sensor;

import java.util.Observable;
import java.util.Random;
import ui.SensorView;

public class AdapteurTemperatureSensor extends Observable implements ISensor {
    private LegacyTemperatureSensor l;
    double value=0;
        
    public AdapteurTemperatureSensor(LegacyTemperatureSensor l) {
        this.l=l;
    }

    @Override
    public void on() {
        if(!l.getStatus())
            l.onOff();
    }

    @Override
    public void off() {
        if(l.getStatus())
            l.onOff();
    }

    @Override
    public boolean getStatus() {
        return l.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        if (l.getStatus()){
            value = l.getTemperature();
            setChanged();
            notifyObservers();
        }
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        if (l.getStatus())
            return value;
        else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }

    @Override
    public void addObserver1(SensorView aThis) {
         this.addObserver(aThis);
    }

}
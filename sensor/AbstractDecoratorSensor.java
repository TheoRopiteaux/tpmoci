/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sensor;

import ui.SensorView;

/**
 *
 * @author Théo
 */
public abstract class AbstractDecoratorSensor implements ISensor {

    ISensor c;
    
    @Override
    public void on() {
        c.on(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void off() {
       c.off(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean getStatus() {
       return c.getStatus(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update() throws SensorNotActivatedException {
        c.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        return c.getValue();
    }

    @Override
    public void addObserver1(SensorView aThis) {
        c.addObserver1(aThis);
    }
    
}

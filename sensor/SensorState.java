/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sensor;

/**
 *
 * @author Théo
 */
public interface SensorState {
	public void on();
	public void off();
	public SensorState getStatus();

    
}

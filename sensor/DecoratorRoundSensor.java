/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sensor;

/**
 *
 * @author Théo
 */
public class DecoratorRoundSensor extends AbstractDecoratorSensor{
   
    public DecoratorRoundSensor(ISensor s) {
        c=s;
    }

    
    @Override
    public double getValue() throws SensorNotActivatedException {
        return Math.round(super.getValue()); 
    }
}

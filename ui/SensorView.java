package ui;

import helpers.ReadPropertyFile;
import sensor.ISensor;
import sensor.SensorNotActivatedException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import sensor.Commande;
import sensor.CommandeGetValue;
import sensor.CommandeOff;
import sensor.CommandeOn;
import sensor.CommandeUpdate;
import sensor.DecoratorFarenheitSensor;
import sensor.DecoratorRoundSensor;
import sensor.Invoker;

public class SensorView extends JPanel implements Observer{
    private ISensor sensor;
   
    private Commande commandeOn;
    private Commande commandeOff;
    private Commande commandeUpdate;
    private Commande commandeGetValue;
    private DecoratorFarenheitSensor df;
    private DecoratorRoundSensor dr;
    private Invoker invoker = new Invoker();
    
    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");
    private JButton celsius = new JButton("Celsius");
    private JButton farenheit = new JButton("Farenheit");
    private JButton round = new JButton("Round");
    
    public SensorView(ISensor c) throws IOException {
        this.sensor = c;
        commandeOn=new CommandeOn(sensor);
        commandeOff=new CommandeOff(sensor);
        commandeUpdate=new CommandeUpdate(sensor);
        commandeGetValue=new CommandeGetValue(sensor);
        df=new DecoratorFarenheitSensor(sensor);
        dr=new DecoratorRoundSensor(sensor);
        
        this.setLayout(new BorderLayout());
        sensor.addObserver1(this);
        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                invoker.setCmd(commandeOn);
                invoker.use();
            }
        });
        
         farenheit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                commandeGetValue.setSensor(df);
                invoker.setCmd(commandeGetValue);
                value.setText(invoker.use()+" F°");
            }
        });
         
          celsius.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                commandeGetValue.setSensor(sensor);
                invoker.setCmd(commandeGetValue);
                value.setText(invoker.use()+" C°");
            }
        });
          
           round.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                commandeGetValue.setSensor(dr);
                invoker.setCmd(commandeGetValue);
                value.setText(invoker.use()+"");
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                invoker.setCmd(commandeOff);
                invoker.use();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { 
                invoker.setCmd(commandeUpdate);
                invoker.use();
                
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 6));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);
        buttonsPanel.add(celsius);
        buttonsPanel.add(farenheit);
        buttonsPanel.add(round);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

    @Override
    public void update(Observable o, Object o1) {
        ISensor o2=(ISensor)o;
        commandeGetValue.setSensor(o2);
        invoker.setCmd(commandeGetValue);
        this.value.setText(invoker.use()+"");
        
    }
}

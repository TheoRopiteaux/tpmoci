package ui;



import helpers.ReadPropertyFile;
import sensor.ISensor;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainWindow extends JFrame {

    private ISensor sensor;
    private SensorView sensorView;
    private JMenuBar m= new JMenuBar();
    private JMenu menu1= new JMenu("Commandes");
    private JMenuItem mi;

    public MainWindow(ISensor sensor) {
        this.sensor = sensor;
        try {
            this.sensorView = new SensorView(this.sensor);
        } catch (IOException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ReadPropertyFile rp=new ReadPropertyFile();
        Properties p;
        try {
            p = rp.readFile("commande.properties");
            for (String i: p.stringPropertyNames()) {
            mi=new JMenuItem(i);
            menu1.add(mi);
        }
            m.add(menu1);
        this.setJMenuBar(m);
        this.setLayout(new BorderLayout());
        
        } catch (IOException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        this.add(this.sensorView, BorderLayout.CENTER);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }


}

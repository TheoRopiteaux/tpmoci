

import ui.MainWindow;
import sensor.AdapteurTemperatureSensor;
import sensor.ISensor;
import sensor.LegacyTemperatureSensor;
import sensor.SensorFactory;
import sensor.SensorProxy;
import sensor.SimpleSensorLogger;
import sensor.TemperatureSensor;

public class SwingApp {

    public static void main(String[] args) {
       //ISensor sensor = new AdapteurTemperatureSensor(new LegacyTemperatureSensor());
        ISensor sensor = SensorFactory.makeSensor();
        new MainWindow(sensor);
    }

}

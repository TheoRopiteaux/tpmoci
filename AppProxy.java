

import sensor.ISensor;
import sensor.SensorProxy;
import sensor.SimpleSensorLogger;
import sensor.TemperatureSensor;
import ui.ConsoleUI;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 18:18
 */
public class AppProxy {
    public static void main(String[] args) {
        ISensor sensor = new SensorProxy(new TemperatureSensor(), new SimpleSensorLogger());
        new ConsoleUI(sensor);
    }
}

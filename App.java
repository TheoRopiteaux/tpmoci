
import sensor.AdapteurTemperatureSensor;
import sensor.Commande;
import sensor.CommandeGetValue;
import sensor.CommandeOff;
import sensor.CommandeOn;
import sensor.CommandeUpdate;
import sensor.ISensor;
import sensor.TemperatureSensor;
import ui.ConsoleUI;
import sensor.ISensor;
import sensor.LegacyTemperatureSensor;
import sensor.TemperatureSensor;

public class App {

    public static void main(String[] args) {
        
        ISensor sensor = new TemperatureSensor();
       
        new ConsoleUI(sensor);
    }

}
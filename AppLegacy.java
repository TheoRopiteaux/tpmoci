
import sensor.AdapteurTemperatureSensor;
import sensor.ISensor;
import sensor.LegacyTemperatureSensor;
import ui.ConsoleUI;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Théo
 */
public class AppLegacy {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ISensor sensor = new AdapteurTemperatureSensor(new LegacyTemperatureSensor());
        new ConsoleUI(sensor);
    }
}
